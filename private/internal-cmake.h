/*$off*/
/*$4 *******************************************************************************************************************
*
* DESCRIPTION:  internal header file
*
* AUTHOR:       robert.berger@reliableembeddedsystems.com
*
* FILENAME:     internal-cmake.h
*
* REMARKS:      this should not go into an external SDK,
*               but only into an internal SDK     
*
* HISTORY:      001b,30 Oct 2019,rber    attempt to put under git
* 		001a,28 Aug 2015,rber    written
*
* COPYRIGHT:    (C) 2019
*
 *********************************************************************************************************************** */
/*$on*/

#ifndef __LIB_INTERNAL_H__
#define __LIB_INTERNAL_H__ 1

/*$3 ===================================================================================================================
    $C                                             Included headers
 ======================================================================================================================= */


/* Included headers before this point */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*$3 ===================================================================================================================
    $C                                                  Macros
 ======================================================================================================================= */

/* differentiate between public and private prototypes */
    #if defined(LIB_INTERNAL_PRIVATE_PROTOTYPES)
        #define EXTERN
    #else
        #define EXTERN  extern
    #endif

/*$3 ===================================================================================================================
    $C                                       Public function declarations
 ======================================================================================================================= */

    EXTERN void internal_receive_byte (void);
    EXTERN void internal_transmit_byte (void);

#undef EXTERN

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LIB_INTERNAL_H__ */

/* 
 * EOF 
 */

