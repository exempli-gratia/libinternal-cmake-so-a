/* lib/lib_hwtest1.c */

#include <stdio.h>
#define LIB_INTERNAL_PRIVATE_PROTOTYPES
#include "private/internal-cmake.h"
#undef LIB_INTERNAL_PRIVATE_PROTOTYPES

void internal_receive_byte () {
   printf ("Internal: Hello, receive_byte\n");
}
